# wheel4_with_button.py (multi-segment touch sensor demo)
# Copyright: 2023 Bradán Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython example uses the TouchWheel library.
It demonstrates a touch wheel composed of four sensors.
It additionally supports a push button composed of a single sensor.

Implementation Notes
--------------------
**Hardware:**
    * the GP pins used will be specific to you hardware board implementation.
    * see the `files` folder for the 'touch_circular_80mm' KiCAD footprint for the touch wheel used with this code.

**Software and Dependencies:**
    * touchwheel library (see 'lib' folder)
    * ulab.numpy is used for array operations and maths for rolling averages to smooth data
"""


import time
import board
from touchwheel import TouchWheel

print("begin")


wheel_sensor_pins = [board.GP22, board.GP23, board.GP24, board.GP25]
button_sensor_pin = board.GP26

touch = TouchWheel(wheel_sensor_pins, button_pin=button_sensor_pin)

print("ready")

prev_button = False

while True:
    count = touch.update()
    button = touch.button
    if button:
        if prev_button:
            prev_button = True
            print ("Button Pressed")
        count -= 1
    elif not button and prev_button:
        prev_button = False
        print ("Button Released")
    if count:
        #print ("Wheel value is {:4d}".format(touch.position))
        print ("({:d}, {:d}, {:d})".format(0, touch.position, touch.limit))    # for Mu plotter
        #print ("{:d} {:d} {:d}".format(0, touch.position, touch.limit))       # for Arduino plotter
        pass
    time.sleep(0.01)
