# TouchWheel

While there should be awesome content in this readme file, sadly there is not.

## Code
The code has been well comented so the best course of action
is to go look at the [touchwheel.py](lib/touchwheel.py) file and then
look at the [example](examples/wheel4_with_button.py).

## Hardware

If you are designing a PCB and want to use a touch wheel, there are two KiCad footprints
in the `files` folder.

<div align="center">![50mm 3-Sensor Touch Wheel](files/touch_circular_50mm.png){width=48%} ![80mm 4-Sensor Touch Wheel](files/touch_circular_80mm.png){width=48%}<br/>each touch wheel footprint also includes a sensor for a button<br/><br/></div>
