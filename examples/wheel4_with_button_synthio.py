# wheel4_with_button.py (multi-segment touch sensor demo)
# Copyright: 2023 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython example uses the TouchWheel library.
It demonstrates a touch wheel composed of four sensors.
It additionally supports a push button composed of a single sensor.

Implementation Notes
--------------------
**Hardware:**
    * the GP pins used will be specific to you hardware board implementation.
    * see the `files` folder for the 'touch_circular_80mm' KiCAD footprint for the touch wheel used with this code.

**Software and Dependencies:**
    * touchwheel library (see 'lib' folder)
    * ulab.numpy is used for array operations and maths for rolling averages to smooth data

There are some fun examples on [GitHub](https://github.com/todbot/circuitpython-synthio-tricks)
"""

print("begin")

# neopixel
import time
import board
from touchwheel import TouchWheel
import synthio
from ulab import numpy

# for PWM audio with an RC filter
import audiopwmio

wheel_sensor_pins = [board.GP22, board.GP23, board.GP24, board.GP25]
button_sensor_pin = board.GP26
neopixel_pin = board.GP10
buzzer_pin = board.GP11

touch = TouchWheel(wheel_sensor_pins, button_pin=button_sensor_pin)
#touch.limit = 12   # use like a clock
#touch.limit = 50   # reasonable range of midi notes
touch.limit = 8     # enough for a music scale
notes = [60, 62, 64, 65, 67, 69, 71, 72]


audio = audiopwmio.PWMAudioOut(buzzer_pin)
synth = synthio.Synthesizer(sample_rate=22050)
audio.play(synth)

SAMPLE_SIZE = 1024
SAMPLE_VOLUME = 32767
sine = numpy.array(numpy.sin(numpy.linspace(0, 2 * numpy.pi, SAMPLE_SIZE, endpoint=False)) * SAMPLE_VOLUME, dtype=numpy.int16)
sawtooth = numpy.linspace(SAMPLE_VOLUME, -SAMPLE_VOLUME, num=SAMPLE_SIZE, dtype=numpy.int16)
#lfo = synthio.LFO(sine, rate=0.1)
#synth.blocks.append(lfo)


env = 0
envelopes = {}
envelopes[0] = synthio.Envelope(attack_time=0.2, release_time=0.8, sustain_level=1.0)
envelopes[1] = synthio.Envelope(attack_time=0.05, release_time=0.2, sustain_level=1.0)
synth.envelope = envelopes[env]  # could also set in synth constructor

note = -1
note1 = None
note2 = None
pressed = False

def button_press_test(count):
    global pressed, env, synth, envelopes
    if touch.button:
        if not pressed:
            pressed = True
            env = (env + 1) % 2
            synth.envelope = envelopes[env]  # could also set in synth constructor
            print("Switching to {} envelope".format(("FAST" if env else "SLOW")))
        count -= 1
    elif pressed:
        pressed = False
    return count

print("ready")

while True:
    count = touch.update()
    if count:
        count = button_press_test(count)
    if count:
        # at least one wheel sensor is active
        #print(touch)
        if (touch.position % 8) != note:
            if note >= 0:
                synth.release(note1)  # release the note we pressed
                synth.release(note2)  # release the note we pressed
            note = touch.position % 8 # wrap notes
            note1 = synthio.Note(synthio.midi_to_hz(notes[note]), waveform=sawtooth)
            note2 = synthio.Note(synthio.midi_to_hz(notes[note]-7), waveform=sawtooth)
            synth.press(note1)
            #time.sleep(0.05)
            synth.press(note2)
            #synth.press(notes[note])  # midi note
            print("Wheel position {:2d} is note {:.0f}Hz ".format(touch.position, synthio.midi_to_hz(notes[note])))
    else:
        if note >= 0:
            synth.release(note1)  # release the note we pressed
            synth.release(note2)  # release the note we pressed
            note = -1

    time.sleep(0.01)
