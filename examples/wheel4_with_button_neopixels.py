# wheel4_with_button.py (multi-segment touch sensor demo)
# Copyright: 2023 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython example uses the TouchWheel library.
It demonstrates a touch wheel composed of four sensors.
It additionally supports a push button composed of a single sensor.

Implementation Notes
--------------------
**Hardware:**
    * the GP pins used will be specific to you hardware board implementation.
    * see the `files` folder for the 'touch_circular_80mm' KiCAD footprint for the touch wheel used with this code.

**Software and Dependencies:**
    * touchwheel library (see 'lib' folder)
    * ulab.numpy is used for array operations and maths for rolling averages to smooth data
"""


import time
import board
from touchwheel import TouchWheel
import neopixel

print("Hello World!")

wheel_sensor_pins = [board.GP22, board.GP23, board.GP24, board.GP25]
button_sensor_pin = board.GP26
neopixel_pin = board.GP10

DC_PIN = board.GP16
CS_PIN = board.GP17
SCK_PIN = board.GP18
MOSI_PIN = board.GP19
RESET_PIN = board.GP20
BUSY_PIN = board.GP21

neopixel_count = 10
pixels = neopixel.NeoPixel(neopixel_pin, neopixel_count, brightness=0.3, auto_write=False)

BLACK = (0,0,0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
YELLOW = (255, 150, 0)
BLUE = (0, 0, 255)
PURPLE = (180, 0, 255)    
CYAN = (0, 255, 255)
WHITE = (255, 255, 255)

colors = [BLACK, RED, GREEN, YELLOW, BLUE, PURPLE, CYAN, WHITE]
color = 1
max_colors = len(colors)

touch = TouchWheel(wheel_sensor_pins, button_pin=button_sensor_pin)
touch.limit = max_colors

while True:
    if touch.update():
        #print(touch)
        if touch.position > 0:
            print("Wheel position: {:d}".format(touch.position))
            pixels.fill(colors[touch.position])
            pixels.show()
        if touch.button:
            print("Button is pressed")
    time.sleep(0.1)
