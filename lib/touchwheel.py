# touchwheel.py (multi-segment touch sensor)
# Copyright: 2023 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython library supports a series of interleaved touch sensors arranged in a circle.

Implementation Notes
--------------------
**Hardware:**
    * see the `files` folder for the KiCAD footrpint for the touch wheel used with this code.
    * the GP pins used will be specific to you hardware board implementation.

**Software and Dependencies:**
    * ulab.numpy is used for array operations and maths for rolling averages to smooth data
"""

import time
import board
from ulab import numpy
import touchio


class TouchWheel:
    AVERAGING_SIZE = 6
    SMOOTHING_SIZE = 10
    THRESHOLD = 50
    LIMIT_DEFAULT = 100

    '''
    TouchWheel Constructor (wheel_pins, button_pin=None):
        the input for the construction must contain the board pins used by the sensors making up the wheel
        the physical wheel sensors progress in a series around the wheel
        the wheel_pins must follow this progression
        the code supports wheels of 3 sensors or more
        there is also support for an optional touch sensor to act as a push button
    '''
    def __init__(self, pins: list, button_pin: int = None):
        self.__all_sensors_count = len(pins)
        self.__wheel_sensors_count = self.__all_sensors_count
        if button_pin:
            self.__all_sensors_count += 1
            pins.append(button_pin)

        self.__sensors  = [0] * self.__all_sensors_count
        self.__readings = [0] * self.__all_sensors_count
        self.__base     = [0] * self.__all_sensors_count
        self.__value    = [0] * self.__all_sensors_count

        for i in range(self.__all_sensors_count):
            self.__sensors[i]   = touchio.TouchIn(pins[i])
            self.__readings[i]  = numpy.zeros(TouchWheel.AVERAGING_SIZE, dtype=numpy.int16)
            self.__base[i]      = 0  # used to zero out the initial base readings
            self.__value[i]     = 0  # used to storage the result of the rolling average
        self.__limit = TouchWheel.LIMIT_DEFAULT

        self.__position = 0
        self.__smoothing = numpy.zeros(TouchWheel.SMOOTHING_SIZE, dtype=numpy.int16)
        self.calibrate()
    # end __init__


    '''
    TouchWheel representation:
        a convenient string representation of the TouchWheel object
    '''
    def __repr__(self):
        out = ""
        for i in range(self.__all_sensors_count):
            out += "{:6d} ".format(self.__value[i])
        out += " ==> {:6d} ".format(self.__position)
        return out
    # end __repr__


    '''
    TouchWheel Calibrate:
        the TouchWheel self calibrates when initialized
        the calibrated values serve as a baseline for the threshold detection
        this function provides a method for force the TouchWheel to re-calibrate
    '''
    def calibrate(self):
        print("Calibration: ", end="")
        for i in range(self.__all_sensors_count):
            for j in range(TouchWheel.AVERAGING_SIZE):
                self.__readings[i][j] = self.__sensors[i].raw_value
                time.sleep(0.05)
            self.__base[i] = int(numpy.mean(self.__readings[i]))
            print ("{:4d} ".format(self.__base[i]), end="")
        print("")
    # end calibrate


    '''
    TouchWheel interpolate (private method):
        compute the value within a range 'limit' based on the strengths of the two endpoints
    '''
    def __interpolate(self, a: int, b: int, limit: int) -> int:
        val = 0
        if not a:
            a = 1
        if not b:
            b = 1
        if a > b:
            val = (b / a) * limit / 2
        else:
            val = limit - ((a / b) * limit / 2)
        return int((val + 0.5))
    # end __interpolate


    '''
    TouchWheel Update:
        read the sensors and store associated data
        returns the number of sensors which are active (aka a value above the threshold)
        the TouchWheel maintains a rolling average of the raw data to eliminate most jitter
        the wheel sensors are used to compute a position value with the configured range limit
        the position value is further smoothed for usability
    '''
    def update(self) -> int:
        touches = 0
        wheel_active = False
        for i in range(self.__all_sensors_count):
            self.__readings[i] = numpy.roll(self.__readings[i], 1)
            self.__readings[i][0] = self.__sensors[i].raw_value
            self.__value[i] = int(numpy.mean(self.__readings[i])) - self.__base[i]
            if self.__value[i] < TouchWheel.THRESHOLD:
                self.__value[i] = 0
            else:
                touches += 1
                # if we are updating one of the wheel sensors, then we raise a flag
                if i < (self.__wheel_sensors_count - 1):
                    wheel_active = True

        if not wheel_active:
            # if we no longer have any wheel sensors active, we clear the position which will later flush the smoothing
            self.__position = 0
            return touches

        # if any of the circle touch pads have data, we compute the position (a value between 0 and LIMIT)
        # we determine which position is the largest; we then determine with adjacent value is next largest
        # the position is calculated from the quadrant and the ratio of those 2 values

        pos_a = 0
        a = 0
        swap = False

        for i in range(1, self.__wheel_sensors_count):
            val = self.__value[i]
            if val > a:
                a = val
                pos_a = i

        # test the before and after values relative the 'a' value
        sector_size = 1.0 / self.__wheel_sensors_count
        if pos_a == 0:
            before = self.__wheel_sensors_count - 1
            after  = 1
        elif pos_a == self.__wheel_sensors_count - 1:
            before = self.__wheel_sensors_count - 2
            after = 0
        else:
            before = pos_a - 1
            after = pos_a + 1
        if self.__value[before] > self.__value[after]:
            pos_b = before
            swap = True
        else:
            pos_b = after

        new_position = 0
        if self.__value[pos_b] == 0:
            # the position is exactly on the 'a' sensor
            new_position = int(self.__limit * (sector_size * (pos_a)))
            #print("calculated {:d}[{:4d}]:{:d}[{:4d}] as {:2d}".format(pos_a, self.__value[pos_a], pos_b, self.__value[pos_b], new_position))
        else:
            if swap:
                # swap positions so calculation is ordered a and b
                temp = pos_b; pos_b = pos_a; pos_a = temp
            base = int(self.__limit * (sector_size * pos_a))
            new_position = self.__interpolate(self.__value[pos_a], self.__value[pos_b], int(self.__limit * sector_size))
            #print("calculated {:d}[{:4d}]:{:d}[{:4d}] as {:2d} within {:2d} from {:2d}".format(pos_a, self.__value[pos_a], pos_b, self.__value[pos_b], new_position, int(self.__limit * sector_size), base))
            new_position += base

        if self.__position == 0:
            # if this is a new position after being idle, we fill the smoothing
            for i in range(TouchWheel.SMOOTHING_SIZE):
                self.__smoothing[i] = new_position

        self.__smoothing = numpy.roll(self.__smoothing, 1)
        self.__smoothing[0] = new_position
        self.__position = int(numpy.mean(self.__smoothing))

        return touches
    # end update


    '''
    TouchWheel button (read-only property):
        returns True is the button is being pressed (aka is above the threshold)
        returns False if there is no button of if it is not pressed (aka is below the threshold)
    '''
    @property  # read-only
    def button(self) -> bool:
        if self.__all_sensors_count != self.__wheel_sensors_count:
            return (self.__value[self.__all_sensors_count-1] != 0)
        return False


    '''
    TouchWheel position (read-only property):
        returns an integer value between 0 and the range-limit
        returns 0 when the wheel sensors are not pressed (aka are below the threshold)
    '''
    @property  # read-only
    def position(self) -> int:
        return self.__position


    '''
    TouchWheel position (read-write property):
        returns the current range-limit used for reporting the position
        assignment will set a new range-limit
    '''
    @property
    def limit(self) -> int:
        return self.__limit

    @limit.setter
    def limit(self, val:int):
        if val > 0:
            self.__limit = val


    '''
    TouchWheel Value(num):
        returns the current computed value for the 'num' sensor
        returns 0 if if then 'num' sensor is below the threshold
    '''
    def value(self, num: int):
        if num >= self.__all_sensors_count:
            return 0
        return self.__value[num]


    '''
    TouchWheel Value(num):
        returns True if the 'num' sensor is active (aka is above the threshold)
        otherwise it returns False
    '''
    def touched(self, num: int):
        if num >= self.__all_sensors_count:
            return False
        return self.__value[num] > (self.__base[num] + TouchWheel.THRESHOLD)

    # end touched
# end Touch Wheel
